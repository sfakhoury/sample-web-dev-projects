namespace BooX.Models
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
 
    public class MainPageModel
    {
        public List<Listing> Listing { get; set; }
        public List<Book> Book { get; set; }
        public Author Author { get; set; }
        public Condition Condition { get; set; }
        public Publisher Publisher { get; set; }
    }
}
