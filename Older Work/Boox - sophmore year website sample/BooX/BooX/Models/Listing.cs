using System.ComponentModel.DataAnnotations;

namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Listing")]
    public partial class Listing
    {
        public Listing()
        {
            Sales = new HashSet<Sale>();
           
        }

        public int Id { get; set; }

        public int Book_ID { get; set; }

        
        public int User_ID { get; set; }

        [Required]
        public int Condition_Id { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        public decimal Price { get; set; }

        //[Column(TypeName = "datetime")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Date_Listed { get; set; }

        [Required]
        public int Status_ID { get; set; }

        [Required]
        public int Course_ID { get; set; }

        public virtual Book Book { get; set; }

        public virtual Condition Condition { get; set; }

        public virtual Status Status { get; set; }

        public virtual Course Course { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        private Model1 db = new Model1();

 

    }
}
