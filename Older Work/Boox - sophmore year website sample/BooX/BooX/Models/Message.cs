namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Message
    {
       
        public int Id { get; set; }

        public int From_UserID { get; set; }

        public int To_UserID { get; set; }

        public int Listing_ID { get; set; }
        [Required]
   [StringLength(1000)]
        public string Content { get; set; }
        [Required]
      [StringLength(100)]
        public string Subject { get; set; }

       
        
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Date { get; set; }
    }
}
