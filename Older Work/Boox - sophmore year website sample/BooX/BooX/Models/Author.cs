namespace BooX.Models
{
    using System;
    using System.Linq;
    using System.Configuration;
    using System.Web.Mvc;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.SqlClient;

    [Table("Author")]
    public partial class Author
    {
        public Author()
        {
            Books = new HashSet<Book>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Author_Name { get; set; }

        [NotMapped]
        public SelectList AuthorList { get; set; }


        public virtual ICollection<Book> Books { get; set; }

    

    }
}
