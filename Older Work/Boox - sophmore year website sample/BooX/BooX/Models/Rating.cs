namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rating
    {
       
        public int Id { get; set; }

        public int User_ID { get; set; }

        public int rating { get; set; }
    }
}
