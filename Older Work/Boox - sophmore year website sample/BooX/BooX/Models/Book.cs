
namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.IO;

    [Table("Book")]
    public partial class Book
    {
        public Book()
        {
        
            Listings = new HashSet<Listing>();
        }

  
        
        public int Id { get; set; }

        
        [StringLength(13)]
        public string ISBN { get; set; }
       
        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        public string Author_Name { get; set; }

        public string Publisher_Name { get; set; }

        public int? Year { get; set; }

        public int? Edition { get; set; }

        [Column(TypeName = "image")]
        public byte[] Image { get; set; }

        public virtual ICollection<Listing> Listings { get; set; }

        private Model1 db = new Model1();

        public List<Book> GetBooks(String id){
            List<Book> books = new List<Book>();
           

           // db.Books.Find(id);
            //(System.Collections.Generic.List<BooX.Models.Book>) db.Books.(id)
            return books;
        }
        public int UploadImageInDataBase(System.Web.HttpPostedFileBase file, Book book)
        {
            book.Image = ConvertToBytes(file);
            var Content = new Book{
                Title = book.Title,
                Author_Name = book.Author_Name,
                Publisher_Name = book.Publisher_Name,
                ISBN =  book.ISBN,
                Edition = book.Edition,
                Year = book.Year,
                Id = book.Id,
               
            };
            db.Books.Add(book);
            int i = db.SaveChanges();
            if (i == 1) return 1;
            else return 0;
        }

        private byte[] ConvertToBytes(System.Web.HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}
