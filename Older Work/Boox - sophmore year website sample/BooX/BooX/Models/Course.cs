namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Course")]
    public partial class Course
    {
        public Course()
        {
            Listings = new HashSet<Listing>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Course_Name { get; set; }

        public virtual ICollection<Listing> Listings { get; set; }
    }
}
