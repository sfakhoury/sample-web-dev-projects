namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        public User()
        {
            Listings = new HashSet<Listing>();
            Sales = new HashSet<Sale>();
        }

        
        public int ID { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?mail\.aub\.edu$", ErrorMessage = "Registration limited to AUB students/faculty")]
        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string Full_name { get; set; }

        [Column(TypeName = "image")]
        public Byte[] Picture { get; set; }

        [Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "Password must be 8 characters long.")]
        public string Password { get; set; }

        [NotMapped]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string ConfirmPassword { get; set; }

        public virtual ICollection<Listing> Listings { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }
    }
}
