namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Department")]
    public partial class Department
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Dept_Name { get; set; }

        public int? Course_ID { get; set; }

        public virtual Course Course { get; set; }
    }
}
