﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace BooX.Models
{

    public class ViewModel
    {
        public IEnumerable<Book> Book { get; set; }
        public IEnumerable<Listing> Listing { get; set; }

    }
}