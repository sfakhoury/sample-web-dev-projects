namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Condition")]
    public partial class Condition
    {
        public Condition()
        {
            Listings = new HashSet<Listing>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        public virtual ICollection<Listing> Listings { get; set; }
    }
}
