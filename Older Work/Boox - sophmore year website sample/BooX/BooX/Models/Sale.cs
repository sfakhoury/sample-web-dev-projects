namespace BooX.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Sale")]
    public partial class Sale
    {
        public int Id { get; set; }

        public int Buyer_ID { get; set; }

        public int Listing_ID { get; set; }

        public double? Price { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Date { get; set; }

        public virtual Listing Listing { get; set; }

        public virtual User User { get; set; }
    }
}
