namespace BooX.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {


        public Model1()
            : base("name=Model1")
        {
        }

        //public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Condition> Conditions { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Listing> Listings { get; set; }
        //public virtual DbSet<Publisher> Publishers { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Author>()
            //    .HasMany(e => e.Books)
            //    .WithOptional(e => e.Author)
            //    .HasForeignKey(e => e.Author_ID);

            //modelBuilder.Entity<Message>()
            //   .Property(e => e.Subject)
            //   .IsFixedLength();

            //modelBuilder.Entity<Message>()
            //    .Property(e => e.Date);
           

            //modelBuilder.Entity<Book>()
            //    .HasMany(e => e.Courses)
            //    .WithOptional(e => e.Book)
            //    .HasForeignKey(e => e.Book_ID);

            modelBuilder.Entity<Book>()
                .HasMany(e => e.Listings)
                .WithRequired(e => e.Book)
                .HasForeignKey(e => e.Book_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Condition>()
                .HasMany(e => e.Listings)
                .WithRequired(e => e.Condition)
                .HasForeignKey(e => e.Condition_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Course>()
                    .HasMany(e => e.Listings)
               .WithRequired(e => e.Course)
                .HasForeignKey(e => e.Course_ID);

            modelBuilder.Entity<Listing>()
                .Property(e => e.Date_Listed);

            modelBuilder.Entity<Listing>()
                .HasMany(e => e.Sales)
                .WithRequired(e => e.Listing)
                .HasForeignKey(e => e.Listing_ID)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Publisher>()
            //    .HasMany(e => e.Books)
            //    .WithOptional(e => e.Publisher)
            //    .HasForeignKey(e => e.Publisher_ID);

            modelBuilder.Entity<Sale>()
                .Property(e => e.Date)
                .IsFixedLength();

            modelBuilder.Entity<Status>()
                .HasMany(e => e.Listings)
                .WithRequired(e => e.Status)
                .HasForeignKey(e => e.Status_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Listings)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.User_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Sales)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.Buyer_ID)
                .WillCascadeOnDelete(false);
        }
    }
}
