﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BooX.Startup))]
namespace BooX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
