﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BooX.Models;


namespace BooX.Controllers
{
    public class ListingsController : Controller
    {
        private Model1 db = new Model1();

        // GET: Listings
        public ActionResult Index()
        {
            var listings = db.Listings.Include(l => l.Book).Include(l => l.Condition).Include(l => l.Status).Include(l => l.User);
            return View(listings.ToList());
        }

        public IQueryable<Listing> list(User user)
        {
             var searchString = 2;
            var Listing = from m in db.Listings select m;

            Listing = Listing.Where(s => s.User_ID.Equals(searchString));
            return Listing;
        }

        // GET: Listings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            return View(listing);
        }

        // GET: Listings/Create
        public ActionResult Create()
        {
            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN");
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type");
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value");
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email");
            return View();
        }

        // POST: Listings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Book_ID,User_ID,Condition_Id,Price,Date_Listed,Status_ID")] Listing listing)
        {
            if (ModelState.IsValid)
            {
                db.Listings.Add(listing);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN", listing.Book_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email", listing.User_ID);
            return View(listing);
        }

        // GET: Listings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN", listing.Book_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email", listing.User_ID);
            return View(listing);
        }

        // POST: Listings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Book_ID,User_ID,Condition_Id,Price,Date_Listed,Status_ID")] Listing listing)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN", listing.Book_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email", listing.User_ID);
            return View(listing);
        }

        // GET: Listings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            return View(listing);
        }

        // POST: Listings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Listing listing = db.Listings.Find(id);
            db.Listings.Remove(listing);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
