﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BooX.Models;
using System.Dynamic;

namespace BooX.Controllers
{
    public class SearchRController : Controller
    {
        private Model1 db = new Model1();
        
        // GET: SearchR
      

        public ActionResult Index(string searchString, string searchBy)
        {
           
            var bks = from m in db.Books select m;
            List<int> bookIDs = new List<int>();
            if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
            {
                if (searchBy == "0")
                {
                    bks = bks.Where(s => s.Title.Contains(searchString));
                }
                else if (searchBy == "1")
                {
                    bks = bks.Where(s => s.Author_Name.Contains(searchString));
                }
                else if (searchBy == "2")
                {
                    bks = bks.Where(s => s.Publisher_Name.Contains(searchString));
                }
                foreach (var b in bks)
                {
                    bookIDs.Add(b.Id);
                }
                //return View(bks);
                return DisplayListing(bookIDs);
            }
            return null;
        }

        
        public ActionResult DisplayListing(List<int> b_ID)
        {
            var lst = from l in db.Listings
                      select l;
            var a = lst.Where(s => b_ID.Contains(s.Book_ID));
            List <Listing> b = new List <Listing>();
            foreach (var i in a)
            {
                if (i.Status_ID == 1)
                {
                    b.Add(i);
                }
            }
            return View(b);
        }


        // GET: SearchR/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: SearchR/Create
        public ActionResult Create()
        {
            ViewBag.Author_ID = new SelectList(db.Books, "Publisher", "Publisher");
            ViewBag.Publisher_ID = new SelectList(db.Books, "Author", "Author");
            return View();
        }

        // POST: SearchR/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ISBN,Title,Author_ID,Publisher_ID,Year,Edition,Image")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(book);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Author_ID = new SelectList(db.Books, "Id", "Author_Name", book.Author_Name);
            ViewBag.Publisher_ID = new SelectList(db.Books, "Id", "Publisher_Name", book.Publisher_Name);
            return View(book);
        }

        // GET: SearchR/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.Author_ID = new SelectList(db.Books, "Id", "Author_Name", book.Author_Name);
            ViewBag.Publisher_ID = new SelectList(db.Books, "Id", "Publisher_Name", book.Publisher_Name);
            return View(book);
        }

        // POST: SearchR/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ISBN,Title,Author_ID,Publisher_ID,Year,Edition,Image")] Book book)
        {
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Author_ID = new SelectList(db.Books, "Id", "Author_Name", book.Author_Name);
            ViewBag.Publisher_ID = new SelectList(db.Books, "Id", "Publisher_Name", book.Publisher_Name);
            return View(book);
        }

        // GET: SearchR/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: SearchR/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Book book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
