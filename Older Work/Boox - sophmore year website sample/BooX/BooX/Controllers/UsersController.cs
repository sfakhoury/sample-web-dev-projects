﻿//using System;
//using System.Data;
//using System.Data.Sql;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;
//using System.Net; 
//using System.Web;
//using System.Web.Mvc;
//using BooX.Models;

//namespace BooX.Controllers
//{

//    public class UsersController : Controller
//    {
       
//        private Model1 db = new Model1();


//        // GET: Users
//        public ActionResult Index()
//        {
            
//            return View(db.Users.ToList());
//        }

//        public ActionResult Register()
//        {

//            return View();
//        }

//        //public ActionResult Login()
//        //{
//        //    return View();
//        //}

//        //public ActionResult Profile(User user)
//        //{
//        //    //    var v = db.Listings.Where(a => a.User_ID.Equals(user.ID)).FirstOrDefault();
//        //    // Session["listings"] = v;
//        //    //var p=  v.Price;

//        //    return View(user);
//        //}

//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult Login(User u)
//        //{

//        //    var v = db.Users.Where(a => a.Email.Equals(u.Email) && a.Password.Equals(u.Password)).FirstOrDefault();
//        //    if (v != null)
//        //    {
//        //        Session["ID"] = u;
//        //        return RedirectToAction("Profile", v);

//        //    }

//        //    return View(u);
//        //}
      

//        //[HttpPost]
//        //[ValidateAntiForgeryToken]
//        //public ActionResult Register(User U)
//        //{
//        //    if (ModelState.IsValid)
//        //    {
                
//        //            //you should check duplicate registration here 
//        //            db.Users.Add(U);
//        //            db.SaveChanges();
//        //            ModelState.Clear();
//        //            U = null;
//        //            ViewBag.Message = "Successfully Registration Done";
                
//        //    }
//        //    return View(U);
//        //}

//        // GET: Users/Details/5
//        public ActionResult DetailsUser(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            User user = db.Users.Find(id);
//            if (user == null)
//            {
//                return HttpNotFound();
//            }
//            return View(user);
//        }

//        // GET: Users/Create
//        public ActionResult CreateUser()
//        {
//            return View();
//        }

//        // POST: Users/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult CreateUser([Bind(Include = "ID,Email,Full_name,Picture,Password, ConfirmPassword")] User user)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Users.Add(user);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            return View(user);
//        }

//        // GET: Users/Edit/5
//        public ActionResult EditUser(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            User user = db.Users.Find(id);
//            if (user == null)
//            {
//                return HttpNotFound();
//            }
//            return View(user);
//        }

//        // POST: Users/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "ID,Email,Full_name,Picture,Password")] User user)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(user).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            return View(user);
//        }

//        // GET: Users/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            User user = db.Users.Find(id);
//            if (user == null)
//            {
//                return HttpNotFound();
//            }
//            return View(user);
//        }

//        // POST: Users/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            User user = db.Users.Find(id);
//            db.Users.Remove(user);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}
