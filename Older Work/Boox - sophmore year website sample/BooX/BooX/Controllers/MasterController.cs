﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BooX.Models;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace BooX.Controllers
{
    public class MasterController : Controller
    {
        private Model1 db = new Model1();
       
        static User loggedInUser = new User();

       public ActionResult Reset()
       {
           return View();
       }

       public ActionResult LogOut()
       {
          loggedInUser.ID = 0;
          loggedInUser.Email = null;
          loggedInUser.Password = null;
          loggedInUser.Full_name = null;

          return RedirectToAction("Index", "Home");
       }
       public ActionResult MessageIndex()
       {
         
           var messages = db.Messages.Where(s => s.To_UserID.Equals(loggedInUser.ID));
           List<Message> Message = new List<Message>();

           foreach( var i in messages)
           {
               Message.Add(i);
           }

           return View(Message);
       }

        public ActionResult displayListing (Listing listing)
       {
           return View(listing);
       }

       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult CreateMessage([Bind(Include = "Id,From_UserID, To_UserID,Listing_ID,Content,Subject,Date")] Message message, int ID)
       {
           if (ModelState.IsValid)
           {
               var Listings = from m in db.Listings select m;
              var lid = Listings.Where(s => s.Id.Equals(ID)).First();
               message.To_UserID = lid.User_ID;
               message.Listing_ID = ID;
               message.From_UserID = loggedInUser.ID;
               db.Messages.Add(message);
               db.SaveChanges();
               return RedirectToAction("MessageIndex");
           }

           return View(message);
       }


       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult Rating([Bind(Include = "Id, User_ID, rating")] Rating rating)
       {
           rating.User_ID = loggedInUser.ID;
           db.Ratings.Add(rating);
           db.SaveChanges();
           return View();
       }

      


       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult replyMessage([Bind(Include = "Id,From_UserID,To_UserID,Listing_ID,Content,Subject,Date")] Message message, Message oldMessage)
       {
           if (ModelState.IsValid)
           {
               message.Listing_ID = oldMessage.Listing_ID;
               message.From_UserID = loggedInUser.ID;
               message.To_UserID = oldMessage.From_UserID;
               db.Messages.Add(message);
               db.SaveChanges();
               return RedirectToAction("MessageIndex");
           }

           return View(message);
       }

       public ActionResult replyMessage(Message message)
       {
           return View(message);
       }
       public ActionResult CreateMessage(Message message)
       {
           return View(message);
       }


        public ActionResult bookIndex(User user)
        {
            var searchString = user.ID;
            var Listings = from m in db.Listings select m;

            Listings = Listings.Where(s => s.User_ID.Equals(searchString));

            var allBooks = from m in db.Books select m;

            List<Book> Books = new List<Book>();

            foreach (var i in Listings)
            {   
                var id = i.Book_ID;
                var toAddBook = allBooks.Where(s => s.Id.Equals(id)).First();

                Books.Add(toAddBook);

            }

            return View(Books.ToList());
        }


      
        // GET: Books/Details/5
        public ActionResult BookDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult CreateB()
        {
                
            return View();
        }

        [HttpPost]
        public ActionResult IndexCreateBU(HttpPostedFileBase file, Listing l, Book bk)
        {
            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var f = l.Book_ID + ".jpg";
                var path = Path.Combine(Server.MapPath("~/Content/BookImages"), f);
                file.SaveAs(path);
            }
            return RedirectToAction("Listing", l);
        }

        public ActionResult Listing()
        {
            ViewBag.Course_ID = new SelectList(db.Courses, "Id", "Course_Name");
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type");
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value");
       
            return View();
        }

        public ActionResult ListingIndex (User user)
        {
            var searchString = user.ID;
            var Listings = from m in db.Listings select m;

            Listings = Listings.Where(s => s.User_ID.Equals(searchString));
            return View(Listings.ToList());
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Listing([Bind(Include = "Id,Book_ID,User_ID,Condition_Id,Price,Date_Listed,Status_ID, Course_ID")] Listing listing)
        {
            if (ModelState.IsValid)
            {
                listing.User_ID = loggedInUser.ID;
                if (listing.User_ID == 0) return View();
                db.Listings.Add(listing);
                db.SaveChanges();
                
                return RedirectToAction("Profile");
            }
            ViewBag.Course_ID = new SelectList(db.Courses, "Id", "Course_Name", listing.Course_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
        
            return View(listing);
           

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditListing([Bind(Include = "Id,Book_ID,User_ID,Condition_Id,Price,Date_Listed,Status_ID, Course_ID")] Listing listing)
        {
            if (ModelState.IsValid)
            {
                listing.User_ID = loggedInUser.ID;
                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListingIndex", loggedInUser);
            }

            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN", listing.Book_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
            ViewBag.Course_ID = new SelectList(db.Courses, "Id", "Course_Name", listing.Course_ID);
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email", listing.User_ID);
            return View(listing);
        }

        public ActionResult EditListing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Listing listing = db.Listings.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            ViewBag.Book_ID = new SelectList(db.Books, "Id", "ISBN", listing.Book_ID);
            ViewBag.Condition_Id = new SelectList(db.Conditions, "Id", "Type", listing.Condition_Id);
            ViewBag.Status_ID = new SelectList(db.Status, "Id", "Value", listing.Status_ID);
            ViewBag.Course_ID = new SelectList(db.Courses, "Id", "Course_Name", listing.Course_ID);
            ViewBag.User_ID = new SelectList(db.Users, "ID", "Email", listing.User_ID);
            return View(listing);
         
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateB([Bind(Include = "Id, ISBN,Title,Author_Name,Publisher_Name,Year,Edition,Image")]  Book book , HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                
                db.Books.Add(book);
                db.SaveChanges();
                Listing l = new Listing();
                l.Book_ID = book.Id;
                return IndexCreateBU(file, l, book);
                
            }

            return View(book);
        }
        public ActionResult Settings ()
        {
            return View(loggedInUser);
        }
        // GET: Books/Edit/5
        public ActionResult BookEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = db.Books.Find(id);
            if (book == null)
            {
                return HttpNotFound();
            }
           
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Route("Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BookEdit([Bind(Include = "Id,ISBN,Title,Author_Name,Publisher_Name,Year,Edition,Image")] Book book)
        {
        
           
           
            if (ModelState.IsValid)
            {
                db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("BookIndex", loggedInUser);
            }
            
            return View(book);
        }


/*
 * 
 * users section
 * 
 */


        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Listing1 (User user)
        {
            var searchString = user.ID;
            var Listings = from m in db.Listings select m;

            Listings = Listings.Where(s => s.User_ID.Equals(searchString));

            return View(Listings.ToList());
        }


        public ActionResult Profile(User u)
        {
            if (loggedInUser == null) return View();
            return View(loggedInUser);   
        }
        
        public ActionResult otherProfile(int userID)
        {
            if (loggedInUser.ID != 0)
            {
                if (userID == loggedInUser.ID)
                {
                    return Profile(loggedInUser);
                }

                var user = db.Users.Find(userID);

                return View(user);
            }
            return null;
        }

        public ActionResult Error() {
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User u)
        {
            var v = db.Users.Where(a => a.Email.Equals(u.Email) && a.Password.Equals(u.Password)).FirstOrDefault();
            if (v != null)
            {
  
                loggedInUser = v;
                return RedirectToAction("Profile");

            }
            else if(v == null)
            {
                return RedirectToAction("Error");
             }
            return null;
        }

        public ActionResult UserIndex(User user)
        {
            var searchString = user.ID;
            var user1 = from m in db.Users select m;

            user1 = user1.Where(s => s.ID.Equals(searchString));
            return View(user1.ToList());
        }

        

        // GET: Users/Create
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IndexCreateUserU(HttpPostedFileBase file, User u)
        {
            if (file != null && file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);
                var f = u.ID + ".jpg";
                var path = Path.Combine(Server.MapPath("~/Content/UserImages"), f);
                file.SaveAs(path);
            }
            return RedirectToAction("Index", "Home"); ;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser([Bind(Include = "ID,Email,Full_name,Picture,Password, ConfirmPassword")] User user, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return IndexCreateUserU(file , user);
            }

            return View(user);
        }


        // //POST: Users/Edit/5
        // //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // //more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser([Bind(Include = "ID,Email,Full_name,Picture,Password, ConfirmPassword")] User user)
        {
            if (ModelState.IsValid)
            {
                user.ID = loggedInUser.ID;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("UserIndex", user);
            }
            return View(user);

        }

        public ActionResult EditUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View();
        }



        //protected override void DisposeUser(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        public ActionResult Settings1()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settings1([Bind(Include = "ID,Email,Full_name,Picture,Password")] User user)
        {
            // user = loggedInUser;
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Profile", user);
            }

            return View(user);
        }

        [AllowAnonymous]
        public ActionResult LostPassword()
        {
            return View();
        }



        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LostPassword(User user)
        {
            String body;
            GMailer.GmailUsername = "BooxApplication@gmail.com";
            GMailer.GmailPassword = "AUBBOOKS";
            using (var sr = new StreamReader(Server.MapPath("~/Views/Master/theEmail.txt")))
            {
                body = sr.ReadToEnd();
            }


            GMailer mailer = new GMailer();
            mailer.ToEmail = user.Email;
            mailer.Subject = "Password Recovery";
            var Looser = from m in db.Users select m;
            var l = Looser.Where(s => s.Email.Equals(user.Email)).FirstOrDefault();
            if (l != null)
            {
                string uN = "a";
                string uE = "e";
                string uP = "b";
                foreach (var x in Looser)
                {
                    uP = x.Password;
                    uN = x.Full_name;
                    uE = x.Email;
                }
                mailer.Body = string.Format(body, uN, uE, uP);
                mailer.IsHtml = true;
                mailer.Send();

                return RedirectToAction("Index", "Home");
            }
            else return RedirectToAction("Error", "Master");
        }
        
         //POST: Books/Edit/5
         //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         //more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        



    }
}
