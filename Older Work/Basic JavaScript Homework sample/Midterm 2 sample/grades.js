var count=2;
window.onload = function(){
	document.getElementById("add1").onclick = addCourse;
	document.getElementById("del1").onclick = deleteCourse;
	document.getElementById("computecumulative").onclick = ComputeCumulativeAvg;
	document.getElementById("computemajor").onclick = ComputeMajorAvg;
		document.getElementById("resetpage").onclick = resetPage;
 };
 
 function addCourse(){
			ComputeCumulativeAvg();
			ComputeMajorAvg();
			var courses=document.getElementById("courses");
			var div=document.createElement("DIV");
   
			var dep=document.createElement("INPUT");
			dep.id="dep"+count;
			dep.name="dep"+count;
			dep.type= "text";
			dep.size="16";
			dep.value="CMPS";
			div.appendChild(dep);
			div.appendChild(document.createTextNode(" "));

			var nb=document.createElement("INPUT");
			nb.id="nb"+count;
			nb.name="nb"+count;
			nb.type= "text";
			nb.size="12";
			div.appendChild(nb);
			div.appendChild(document.createTextNode(" "));

			var gr=document.createElement("INPUT");
			gr.id="gr"+count;
			gr.name="gr"+count;
			gr.type= "text";
			gr.size="9";
			gr.value="40";
			div.appendChild(gr);
			div.appendChild(document.createTextNode(" "));
			
			var cr=document.createElement("INPUT");
			cr.id="cr"+count;
			cr.name="cr"+count;		
			cr.type= "text";
			cr.size="11";
			cr.value="3";
			div.appendChild(cr);
			div.appendChild(document.createTextNode(" "));

			var del=document.createElement("INPUT");
			del.id="del"+count;
			del.type= "button";
			del.name=""+count;
			del.value="X"
			del.onclick=deleteCourse;
			div.appendChild(del);
			div.appendChild(document.createTextNode(" "));

			 var add=document.createElement("INPUT");
			add.id="add"+count;
			add.type= "button";
			add.name=""+count;
			add.value="+"
			add.onclick=addCourse;
			div.appendChild(add);
			div.id=""+count;
			//courses.appendChild(div);
			courses.insertBefore(div, document.getElementById(this.name).nextElementSibling);

			count++;
 }
 
  function deleteCourse(){
            var courses = document.querySelectorAll("#courses div");
			var elem=document.getElementById(this.name); 
			if (elem.id > 1){
				elem.parentNode.removeChild(elem);	
				ComputeCumulativeAvg();
				ComputeMajorAvg();
			}
			
  
  }
 

  
    function ComputeCumulativeAvg(){
            var courses = document.querySelectorAll("#courses div");
			var sumcredits=0;
			var sumgrades=0;
			for (var i = 0; i < courses.length; i++) {
				var id = courses[i].id;
				var gr=document.getElementById("gr"+id).value;    
				var cr=document.getElementById("cr"+id).value; 
				sumgrades += parseInt(gr)*parseInt(cr);
				sumcredits += parseInt(cr);
		   }
			var totalcum=document.getElementById("totalcum");
			totalcum.value= parseFloat(sumgrades/sumcredits);
  }
  
    
  
   function ComputeMajorAvg(){
			var major = document.getElementById("major").value;
            var courses = document.querySelectorAll("#courses div");
			var sumcredits=0;
			var sumgrades=0;
			for (var i = 0; i < courses.length; i++) {
				var id = courses[i].id;
				var dep=document.getElementById("dep"+id).value;
				if (dep == major){
					var gr=document.getElementById("gr"+id).value;    
					var cr=document.getElementById("cr"+id).value; 
				    sumgrades += parseInt(gr)*parseInt(cr);
				    sumcredits += parseInt(cr);
				}
		   }
			var totalmaj=document.getElementById("totalmaj");
			totalmaj.value= parseFloat(sumgrades/sumcredits);
  }
  
   function resetPage(){
            var courses = document.querySelectorAll("#courses div");
			for (var i = 0; i < courses.length; i++) {
			    if (courses[i].id>1)
					courses[i].parentNode.removeChild(courses[i]);
		   }
		   count=2;

  }
  




