/* Sarah Fakhoury
Fifteen Puzzle
CMPS 278
March 2016


tested using chrome 
*/


  //location of the empty piece
  var x = 300;
  var y = 300;
var count = 0;

  window.onload = function f() {
	  	  		  
   //initializing three puzzle images
	for (var z = 1; z <= 3; z++)
		 {
		  var img = document.createElement("img");
		  img.setAttribute("src", "img"+ z+ ".jpg");
		  img.style.width = "100px";
			  img.style.height = "100px";
			
		  document.getElementById("images").appendChild(img);
		  img.onclick = bk;
			
		 }
			
	//put the initialize after setting the images so that they can be cleared later on 
    initialize();
	
    document.getElementById("shufflebutton").onclick = shuffle;
  }

  // create the puzzle area
  function initialize() {
	 
	  var v =0;
	  
	  var p = document.getElementById("puzzlearea");
	  p = p.getElementsByTagName("div");
	 
	  for (var i = 0; i < p.length  ; i++)  {
		//var v keeps track of the columns, this could be done using a double array
		//but i thought this was clearer
		
		if (i> 3 )
		{
			v=1;
		
			if(i>7)
			{
				v=2;
				
			}
			if (i>11)
			{
				v=3;
				
			}
		}
		
		 //v is the column and i%4 is the row
        p[i].id = "p_" +v + "_" + (i%4);
		
        p[i].className = "puzzlepiece";
        p[i].style.left = parseInt((i%4) *100) + "px";
		p[i].style.top = parseInt(v * 100) + "px";
		
       p[i].style.backgroundPosition = -parseInt((i%4) * 100) + "px " + -parseInt((v) * 100) + "px" ;
       
			p[i].onmouseover = colorize;
        p[i].onclick = move;
  
	   
      }
	
	  
    
  }

  function bk(img){
	  //chnages the background image
	  img = img.target;
	  for (var i=0; i<15; i++)
	  {
		document.getElementsByClassName("puzzlepiece")[i].style.backgroundImage= "url('"+ img.src + "')";
	  }
	  //call initialize each time the background changes
	  initialize();
  }
  
  // see if you can move a piece
  function canMove(p) {
    if (p == null) {
		      return false;
    }
   
    var current_x = parseInt(p.style.left);//get its current position
    var current_y = parseInt(p.style.top);
	
	//either x or y is going to change when a piece moves
	// if x changes it will either move left or right (positive or negative)
	// and same can be said for y
	//there are four possible ways for the empty square to be in relation to the clicked one
	//i seperate these two by testing if the empty is in the same row or column
	//then within each case i check again if it is to the right/left or up/down 
	//once i find the empty piece it returns true
	//else it will return false because the empty piece is not there
	
	
	var sameRow = (y == current_y && (x == current_x + 100 || x == current_x - 100));
	var sameColumn = (x == current_x && (y == current_y + 100 || y == current_y - 100));
	
    return ( sameRow || sameColumn );
  }
function colorize()
  {
	  if(canMove(this))
	  {
		  this.className +=" movablepiece";
	  }
	  
	  else{
		  this.className = "puzzlepiece";
	  }
  }
  //a function to shuffle the puzzle to play the game
  function shuffle() {

    for (var i = 0; i < 100; i++) {
      
	  //create an array of all the pieces near the empty square
	  // which is at 300, 300 with global vars x and y 
      // Array is created inside the loop because the pieces around the empty square
	  // will change as its location changes and we need to put different pieces
	  //in the array each time
	  
	  //so we will try and get all the surrounding pieces and then randomly move one of the four
     
	 var up = document.getElementById("p_" + ((y / 100) + 1) + "_" + x / 100);
      var side1 = document.getElementById("p_" + y / 100 + "_" + ((x / 100) - 1));
      var down = document.getElementById("p_" + ((y / 100) - 1) + "_" + x / 100);
	  var side2 = document.getElementById("p_" + y / 100 + "_" + ((x / 100) + 1));
		
	var nearEmpty = [];
		
	  //you have to check if the pieces we found actually exist
	  // if empty piece is on an edge some pieces will be null
      if (side1 != null) {
        nearEmpty.push(side1);
      } 
     if (side2 != null) {
        nearEmpty.push(side2);
      } 
      
     if (up != null) {
        nearEmpty.push(up);
      } 
     if (down != null) {
        nearEmpty.push(down);
      }

	  //create random index of array to use
		var random = Math.floor(Math.random() * (nearEmpty.length));
      move(nearEmpty[random]);

    }

  }

  // a function to let people click the movable piece
  function move(p) {
	// the logic in this function works on the fact that the first piece you move
	// will ALWAYS be taking over the empty piece's location at 300 300
	//and then the empty piece's location is updated
	  
  //check if the tripper was clicking
    if (p.type == 'click') {
		//if yes set that object as our target puzzle piece to move
      p = p.target;
    }
    if (canMove(p)) {
      var update_x = parseInt(p.style.left);
      var update_y = parseInt(p.style.top);
		//get the new coordinates of where the piece is trying to move
	 
		// send the clicked piece to 300, 300 which is the location of the empty piece
      p.style.left = x + 'px';
		p.style.top = y + 'px';
	  //update the id of the piece to the empty piece at first
      p.id = "p_" + y / 100 + "_" + x / 100;

      x = update_x;
      y = update_y;
	  //now change x and y to the new empty square coordinates
	 

    }
  }


   
  
