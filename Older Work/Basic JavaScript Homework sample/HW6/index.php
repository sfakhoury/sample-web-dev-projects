<!DOCTYPE html>
<html>
	<head>
		<title>My Movie Database (MyMDb)</title>
		<meta charset="utf-8" />
		
		<!-- Links to provided files.  Do not edit or remove these links -->
		<link href="images/mymdb_icon.gif" type="image/gif" rel="shortcut icon" />
		<script src="search.js" type="text/javascript"></script>


		<!-- Link to your CSS file that you should edit -->
		<link href="bacon.css" type="text/css" rel="stylesheet" />
	</head>

	<body>
		<div id="frame">
			<div id="banner">
				<a href="index.php"><img src="images/mymdb.png" alt="banner logo" /></a>
				My Movie Database
			</div>

			<div id="main">
				<div id="dumpdiv">
					<h1>The One Degree of Kevin Bacon</h1>
					<p>Type in an actor's name to see if he/she was ever in a movie with Kevin Bacon!</p>
					<p><img src="images/kevin_bacon.jpg" alt="Kevin Bacon" /></p>
				</div>
				
					
					<fieldset>
						<legend>All movies</legend>
						<div>
							<input id="fname" name="firstname" type="text" size="12" placeholder="first name" autofocus="autofocus" /> 
							<input id="lname" name="lastname" type="text" size="12" placeholder="last name" /> 
							<input id="searchJSON" type="button" value="go JSON" />
							<input id="searchXML" type="button" value="go XML" />
						</div>
					</fieldset>
					
				
					<fieldset>
						<legend>Movies with Kevin Bacon</legend>
						<div>
							<input id="fnamek" name="firstname" type="text" size="12" placeholder="first name" /> 
							<input id="lnamek" name="lastname" type="text" size="12" placeholder="last name" /> 
							<input id="searchkevin" type="button" value="go" />
						</div>
					</fieldset>
				
			</div> <!-- end of #main div -->
		
			<div id="w3c">
				<a href="http://validator.w3.org/check/referer"><img src="images/w3c-html.png" alt="Valid HTML5" /></a> 
				<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="images/w3c-css.png" alt="Valid CSS!" /></a>
			</div> 

			</div> <!-- end of #frame div -->
	</body>
</html>
