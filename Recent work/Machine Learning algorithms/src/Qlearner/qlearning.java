package Qlearner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class qlearning {

	// Within the gridworld the directions are modeled at 4 actions
	//up direction = 1 right = 2 down =3 left =4

	// update Q(s,a) = previousValue + alpha(immediate Reward 
	//                 + Beta ( maximum reward from all the potential states) - current state's value 
	
	//R is the reward matrix replicating the gridworld
	static double [][] R;
	
	//Q is the learned matrix
	static double [][] Q;
	static double Beta = 0.9;
	static double alpha = 0.01;
	
	//run time too long for low epsilon and high iterations
	static double epsilon = .8;

	// moving up means new state is current state i -=10
	//right -->  i+=1  left --> i-= 1  
	//down --> i-= 10

	public static Double getRand()
	{
		ArrayList<Double> nums = new ArrayList<Double>();
		double factor = epsilon*10;
		for(double i =0; i<10; i++)
		{
			if(i < factor)
			{
				nums.add(0.0);
			}
			else{
				nums.add(1.0);
			}
		}

		double rand = Math.random() *10;
		return nums.get((int) rand);

	}
	//boltzmann 
	public static double[] getNextStateGLIE2(double current)
	{
		int T = 1;
		int currentState = (int)current;
		ArrayList<Integer> states =  new ArrayList <Integer>();
		for(int i=0; i<4; i++)
		{
			double x = R[currentState][i];
			if(x != -100){
				states.add(i);
			}
		}
		int probability= 0;
		int maxVal = -1000;
		int state =0;
		int sum=0;
		for(int i=0; i< states.size(); i++)
		{
			sum += (int) (Math.exp(R[currentState][states.get(i)]/ T));
		}
		for(int i=0; i< states.size(); i++)
		{
			probability = (int) (Math.exp(R[currentState][states.get(i)]/ T))/ sum;
			if (maxVal < probability)
			{
				maxVal = probability;
				state = states.get(i);
			}
		}
		
		double [] results = new double [2];
		
			if(state == 0){
				results[0] = currentState - 10; results[1] = 0;
			}
			else if(state == 1){
				results[0] = currentState + 1; results[1] = 1;
			}
			else if(state == 2){
				results[0] = currentState + 10; results[1] =  2;
			}
			else if(state == 3){
				results[0] =  currentState - 1; results[1] = 3;
			}
			
			return results;
	}
	
	public static double[] getNextStateGLIE1 (double current)
	{
		int NextState;
		int currentState = (int)current;
		//System.out.println(currentState);

		ArrayList<Integer> states =  new ArrayList <Integer>();

		for(int i=0; i<4; i++)
		{
			double x = R[currentState][i];
			if(x != -100){
				states.add(i);
			}
		}
		double choice =  getRand();
		double maxVal= -1000;
		for(int i=0; i< states.size(); i++)
		{
			if(maxVal < R[currentState][states.get(i)])
			{
				maxVal = states.get(i);
			}
		}
		states.remove(maxVal);
		double [] results = new double [2];
		if (choice == 1)
		{

			if(maxVal == 0.0){
				results[0] = currentState - 10; results[1] = 0;
			}
			if(maxVal == 1.0){
				results[0] = currentState + 1; results[1] = 1;

			}
			if(maxVal == 2.0){
				results[0] = currentState + 10; results[1] =  2;
			}
			if(maxVal == 3.0){
				results[0] =  currentState - 1; results[1] = 3;
			}
		}
		else{
			int rand0 = states.get((int) (Math.random() * states.size()));

			if(rand0 == 0){
				results[0] =  currentState - 10; results[1] = 0;
			}
			if(rand0 == 1){
				results[0] = currentState + 1; results[1] = 1;
			}
			if(rand0 == 2){
				results[0] = currentState + 10; results[1] = 2;
			}
			if(rand0 == 3){
				results[0] = currentState - 1; results[1] = 3;
			}
		}
		//System.out.println(results[0]+ " direction "+ results[1]);
		return results;

	}

	public static void initializeQ ()
	{
		R = new double [100][4];
		Q = new double [100][4];
		//initialize entire array to 0
		for(int i = 0; i<100 ; i++)
		{
			for(int j=0 ; j<4; j++){
				R[i][j]= 0;
				Q[i][j]= 0;
			}
		}
		//left column cannot move left direction off the grid
		for(int i = 0; i<=90 ; i+=10)
		{
			R[i][3]= -100;			
		}
		//right column cannot move left off the grid
		for(int i = 9; i<=99; i+=10)
		{
			R[i][1]= -100;			
		}
		//top row cannot move up off the grid
		for(int i = 0; i<=9; i++)
		{
			R[i][0]= -100;			
		}
		//bottom row cannot move down off the grid
		for(int i =90; i<=99; i++)
		{
			R[i][2]= -100;			
		}

		//specific known rewards scattered
		R[63][2] = -1; R[72][1] = -1; R[83][0] = -1;
		R[65][2] = -1; R[85][0] = -1; R[76][3] = -1;
		R[66][2] = -1; R[86][0] = -1; R[77][3] = -1; R[75][1] = -1;
		R[58][2] = -1; R[78][0] = -1; R[69][3] = -1; R[67][1] = -1;
		R[48][2] = -1; R[68][0] = -1; R[59][3] = -1; R[57][1] = -1;
		R[32][1] = -1; R[43][0] = -1;

		//four squares with goal state
		R[45][2] = 100; R[65][0] = 100; R[56][3] = 100; 
		R[35][2] = -1; R[55][0] = -1; R[46][3] = -1; 
		R[36][2] = -1; R[56][0] = -1; R[47][3] = -1; R[45][1] = -1; 
		R[46][2] = -1; R[66][0] = -1; R[57][3] = -1; R[55][1] = -1; 
	}


	public static void runMe()
	{
		initializeQ();
		int currentState = 0;
		int currentDirection = 0;
		int nextState;
		int direction;
		int nextDirection;
		int counter= 0;
		while(counter < 50000)
		{
//choose glie2 for boltzman
			nextState =(int) getNextStateGLIE1 (currentState)[0];
			direction = (int) getNextStateGLIE1 (currentState)[1];
			nextDirection = (int) getNextStateGLIE1 (nextState)[1];
			Q[currentState][currentDirection] += alpha * (R[currentState][currentDirection] + Beta * (Q[nextState][nextDirection]) - Q[currentState][currentDirection]);
			currentState= nextState;
			currentDirection = direction;

			if(currentState == 55)
			{

//				Q[currentState][currentDirection] += alpha * (R[currentState][currentDirection] + Beta * (Q[nextState][direction]) - Q[currentState][currentDirection]);
//
				currentState = 0;
				currentDirection = (int) getNextStateGLIE1 (0)[1];
				counter++;
			}
		}
		System.out.println("I MADE IT in: " + counter+ " iterations");
	}

	public static void main(String args[]) {

		runMe();
		// TODO Auto-generated constructor stub


						for(int i = 0; i<=99; i++)
						{ 
							System.out.print(" [ ");
							
							for (int j=0; j<4; j++){
							System.out.print((int)Q[i][j]+", ");
							}
							
							System.out.println("]" + i);
						}
	}

}
