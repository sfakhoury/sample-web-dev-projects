package EM;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	//CODE WRITTEN IS PARTIALLY INSPIRED BY THE WORK AND EXPLAINATIONS
	//OF THE AUTHOR IN THE FOLLOWING REPOSITORY
	// github.com/harry1357931/GaussianMixtureModel-EM_Algorithm
	
	 
	public static void main(String[] args) throws IOException { 

////////////////PRE PROCESSING////////////
		
		//relative path will work too
		/* 
		 *    "./input/input.txt"
		 */
		String fileName = "C:\\Users\\sarah\\workspace\\ml5\\src\\input\\input.txt";
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		String line = br.readLine(); 
		
		
		double[] inputData = new double[6000];
		int count = 0;
		
		
		while(line!=null)
		{
			//populate data array
			inputData[count] =  Double.parseDouble(line);
			//get next line
			line = br.readLine();
			count++;
		}

		// Build EM_GMM
		System.out.println("Parameters for K = 3");
		EM_GMM run1 = new EM_GMM(inputData, 3);             
		run1.GMM_1(); // init             
		run1.print(); 
		
		System.out.println("Parameters for K = 4");
		EM_GMM run2 = new EM_GMM(inputData, 4);             
		run2.GMM_1();             
		run2.print();  
		
		System.out.println("Parameters for K = 5");
		EM_GMM run3 = new EM_GMM(inputData, 5);             
		run3.GMM_1();             
		run3.print();  
	} 
}
