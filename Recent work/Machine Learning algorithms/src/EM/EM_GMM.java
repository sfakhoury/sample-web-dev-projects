package EM;
public class EM_GMM {
	
	//user k
	public int k = 0;
	
	public double[] data_;
	
	//parameters
	public double[] alpha;
	public double[] mu;
	public double[] beta;
	
	public void GMM_1(){

		param_init(k);
		//convergence control value
		double 	delta = 1;
		
		while(delta > 0.000001)
		{
			double LogLikelihood=0, ProbOfXn=0;

			
			for(int i=0; i< data_.length; i++){
				ProbOfXn = 0;
				for(int j = 0; j < k; j++){            
					ProbOfXn = ProbOfXn + alpha[j]*Gaussian(data_[i], mu[j], beta[j]);  		
				}	
				LogLikelihood = LogLikelihood + Math.log1p(ProbOfXn);
			}

			///////////////////////////////////////
			/// estimation ///
			
			double[][] probability = new double[data_.length][k]; 
			for(int i=0; i< data_.length; i++){
				ProbOfXn = 0;
				for(int j = 0; j < k; j++){
					probability[i][j] = alpha[j]*Gaussian(data_[i], mu[j], beta[j]);
					ProbOfXn = ProbOfXn + probability[i][j]; 
				}
				for(int j = 0; j < k; j++){
					probability[i][j] = probability[i][j]/ProbOfXn;
				}
			}

			////////////////////////////
			/// maximization ///
			
			double[] N_k = new double[k];
			for(int i = 0; i < k; i++){ 
				for(int n=0; n< data_.length; n++){
					N_k[i] = N_k[i] + probability[n][i];
				}
			}

	///////////////////////////////////
			//update mu///
			for(int i = 0; i < k; i++){
				mu[i] = 0 ;
				for(int n = 0; n < data_.length; n++){
					mu[i] = mu[i] + probability[n][i]*data_[n];
				}
				mu[i] = mu[i]/N_k[i] ;
			}

			//////////////////////////////////////
			///update beta prediction//// 
			for(int i = 0; i < k; i++){
				beta[i] = 0;
				for(int n=0; n< data_.length; n++){
					beta[i] = beta[i] + probability[n][i]*(data_[n]-mu[i])*(data_[n]-mu[i]);
				}
				beta[i] = beta[i]/N_k[i];
			}

			///////////////// update alpha
			for(int i = 0; i < k; i++){
				alpha[i] = N_k[i]/data_.length;
			}

			double NewLL=0, P=0;

			
			for(int i = 0; i < data_.length; i++){
				P = 0;
				for(int j = 0; j < k; j++){            
					P = P + alpha[j]*Gaussian(data_[i], mu[j], beta[j]);  		
				}	
				NewLL = NewLL + Math.log1p(P);
			}

			delta = NewLL - LogLikelihood;
		}
	}
	
public EM_GMM(double[] inputData, int k){                    
		
		this.mu = new double[k];
		this.alpha = new double[k]; 
		this.beta = new double[k];
		this.data_ = inputData;
		this.k = k;
		
	}

///Initial guesses, random but here they were given close to optimal
//in each cluster set
//in interest of TA testing time
	public void param_init(int k){

		if (k == 3){
			
			beta[0] = 5;
			beta[1] = 3;
			beta[2] = 3;
			alpha[0] = 0.33;                              
			alpha[1] = 0.33;                              
			alpha[2] = 0.34;                              
			mu[0] = 5;                               
			mu[1] = 15;                               
			mu[2] = 25;                                
		}

		if (k == 4){
			alpha[0] = 0.33;                              
			alpha[1] = 0.33;                              
			alpha[2] = 0.34; 
			alpha[3] = 0.34;        
			

			mu[0] = 4;                               
			mu[1] = 6;                               
			mu[2] = 15;
			mu[3] = 25;

			beta[0] = 5;
			beta[1] = 3;
			beta[2] = 3;
			beta[3] = 5;
		}
		
		if (k == 5){
			alpha[0] = 0.33;                              
			alpha[1] = 0.33;                              
			alpha[2] = 0.34; 
			alpha[3] = 0.34;        
			alpha[4] = 0.35;    
			mu[0] = 4;                               
			mu[1] = 6;                               
			mu[2] = 15;
			mu[3] = 25;
			mu[4] = 25;
			beta[0] = 5;
			beta[1] = 3;
			beta[2] = 3;
			beta[3] = 5;
			beta[4] = 3;
		}
	}
	
	public double Gaussian(double x_n, double Mu_k , double Sigma_k ){     // return N(x_n|...)

		double probability = 0;
		probability = Math.pow(2*Math.PI*Sigma_k ,-0.5)*Math.exp(-(Math.pow(x_n-Mu_k, 2))/(2*Sigma_k)); 
		return probability;
	}

	
	public void print(){
		for(int i = 0; i < k; i++)
			System.out.println("Cluster # " + (i + 1) +"\nAlpha = " + alpha[i] + "\nMu = " + mu[i] +"\n Beta = "+ beta[i]+"\n");	
	}
}
