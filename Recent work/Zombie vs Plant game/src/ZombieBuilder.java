
/**
 * @stereotype INTERFACE 
 */
abstract interface ZombieBuilder {

	/**
	 * @stereotype ABSTRACT 
	 */
	abstract void CreateZombie();
	/**
	 * @stereotype ABSTRACT 
	 */
	abstract void CreateAccesory();		
	/**
	 * @stereotype ABSTRACT 
	 */
	public ZombieProduct zombie();
}
