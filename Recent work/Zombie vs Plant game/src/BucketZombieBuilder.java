
/**
 * @stereotype BOUNDARY 
 */
public class BucketZombieBuilder implements ZombieBuilder {
	Zombie z;
	Bucket bucket;
	
	/**
	 * @stereotype SET COLLABORATOR 
	 */
	public void CreateZombie() {
		Zombie z = new Zombie();
		this.z = z;
		
	}

	/**
	 * @stereotype SET COLLABORATOR 
	 */
	@Override
	public void CreateAccesory() {
		Bucket bucket = new Bucket();
		this.bucket = bucket;
		
	}

	
	/**
	 * @stereotype FACTORY COLLABORATOR 
	 */
	public ZombieProduct zombie() {
		
		ZombieProduct product = new ZombieProduct(this.bucket, this.z);
		
		return product;
	}

	
}
