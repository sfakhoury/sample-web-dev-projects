
/**
 * @stereotype DATA_PROVIDER 
 */
public class Director {
ZombieBuilder b;
	
	/**
	 * @stereotype CONSTRUCTOR COLLABORATOR 
	 */
	public Director(ZombieBuilder b) {
		this.b = b;
		b.CreateZombie();
		b.CreateAccesory();
	
	}
	
	/**
	 * @stereotype PROPERTY 
	 */
	public ZombieProduct construct()
	{
		return b.zombie();
		
	}

}
