
/**
 * @stereotype COMMANDER 
 */
public class ZombieProduct extends Zombie{

	int Health;
	String name;
	AccessoryComponent acc;
	Zombie zom;
	boolean dead = false;
	
	
	/**
	 * @stereotype CONSTRUCTOR 
	 */
	public ZombieProduct(AccessoryComponent a, Zombie z) {
		this.acc = a;
		this.zom =z;
		if(acc != null)
		this.name = acc.name + "Z";
		else
			this.name="RZ";
		
		if(acc!=null)
		this.Health = acc.health + zom.Health;
		else
			this.Health = zom.Health;
	}

	/**
	 * @stereotype COMMAND LOCAL_CONTROLLER 
	 */
	public void takeDamage(int dmg)
	{
		
		
		if (acc!= null && acc.protect(dmg))
		{
			int overflow= dmg - acc.health;
			this.Health = acc.health + zom.Health;
			if (!acc.protect(0))
			{
				if(overflow >=0)
				this.name = "RZ";
				
				zom.takeDamage(overflow);
			}
		}
		else{
			zom.takeDamage(dmg);
			
			this.Health = zom.Health;
			
			if(zom.die())
			{
				dead= true;
			}
		}
	}
}

