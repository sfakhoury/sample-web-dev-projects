
/**
 * @stereotype COMMANDER 
 */
public class Bucket  extends AccessoryComponent{
	
	boolean dead = false;
	
	/**
	 * @stereotype CONSTRUCTOR 
	 */
	public Bucket() {
		this.health = 100;
		this.name = "B";
	}

	

	/**
	 * @stereotype SET 
	 */
	@Override
	public boolean die() {
		if (health <= 0){
			dead = true;
			return true;
		}
		return false;
	}



	/**
	 * @stereotype SET LOCAL_CONTROLLER 
	 */
	public boolean protect(int dmg) {
		
		if(die())
			return false;
		
		this.health -= dmg;
		
		return true;
	}








}
