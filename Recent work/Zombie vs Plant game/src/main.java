import java.util.ArrayList;
import java.util.Scanner;

/**
 * @stereotype BOUNDARY COMMANDER 
 */
public class main {
	static ZombieProduct z;

	static int dmg = 25;
	static ArrayList<ZombieProduct> ActiveZombies = new ArrayList<ZombieProduct>();
	
	/**
	 * @stereotype SET COLLABORATOR 
	 */
	public static void main(String [] args) throws InterruptedException {
		
		
	    	 
	      Scanner in = new Scanner(System.in);
	 
	      System.out.println("--------------------------------------Welcome to Zombie vs. Plants! The best game on the planet--------------------------------------");
	      Thread.sleep(2000);
	      System.out.println("\t\t\t\tTo play you can either create a zombie or shoot zombies with a Pea Shooter");
	      Thread.sleep(2000);
	      System.out.println(" \n\t\t\t\t\t\t ~~( > w < )~~ Have Fun! ~~( ^ w ^ )~~ \n");
	      Thread.sleep(2000);
	     
	      while(true)
	      {
		      System.out.println("What do you want to do?");
		     
		      
	    	  System.out.println(" 1. Create Zombies \n 2. Demo game play");
	    	  if(in.hasNextInt())
	    	  {
		    	  int choice = in.nextInt();
		    	  if(choice == 1)
		    		  {
		    		  	System.out.println("You chose to Create a Zombie!");
		    	  		
		    		  	enterCreationMode(in);
		    	  		
		    	  	
		    		  }
		    	  
		    	  else if (choice == 2){
		    		  System.out.println("You chose to demo game play!");
		    		  System.out.println("Set Damage Level");
		    		  dmg = in.nextInt();
		    		  Demo(in);
		    	  }
		    	  
		    	  else
		    		  System.out.println("Invalid input, please enter either (1) or (2)");
		    	  
	    	  }
	    	  else{
	    		  in.next();
	    		  System.out.println("Invalid input, please enter either (1) or (2)");
	    	  }
	      }
	}

	/**
	 * @stereotype COLLABORATOR 
	 */
	public static int enterCreationMode(Scanner in)
	{
		while (true)
		{
		
			 System.out.println("What Kind of Zombie do you want to make?");
		     
		      
	    	  System.out.println(" 1. Regular Zombie \n 2. ConeHead Zombie \n 3. BucketHead Zombie\n 4.Door Zombie\n 5.Back to main ");
	    	  if(in.hasNextInt())
	    	  {
	    		  int choice = in.nextInt();
	    		  if(choice==5)
	    			  return 0;
	    		  else
		    	  createZombie(choice);
	    	  }
		}
	}
	
	/**
	 * @stereotype COMMAND COLLABORATOR 
	 */
	public static void createZombie(int type)
	{
		
		if(type == 1){
			RegularZombieBuilder b = new RegularZombieBuilder();
  			Director d = new Director(b);
  			z = d.construct();
  			ActiveZombies.add(z);
		}
		else if(type == 2){
			ConeZombieBuilder b = new ConeZombieBuilder();
  			Director d = new Director(b);
  			z = d.construct();
  			ActiveZombies.add(z);
		}
		else if(type == 3){
			BucketZombieBuilder b = new BucketZombieBuilder();
  			Director d = new Director(b);
  			z = d.construct();
  			ActiveZombies.add(z);
		}
		else if(type == 4){
			DoorZombieBuilder b = new DoorZombieBuilder();
  			Director d = new Director(b);
  			z = d.construct();
  			ActiveZombies.add(z);
		}
		  		
	}
	
	/**
	 * @stereotype NON_VOID_COMMAND COLLABORATOR 
	 */
	public static int Demo(Scanner in)
	{ 
		while(true)
		{
			System.out.print("[");
			for(int i=0; i<ActiveZombies.size(); i++)
				{
					System.out.print("// Zombie " + ActiveZombies.get(i).name +" "+ ActiveZombies.get(i).Health +" //");
				}
			System.out.println("]");
			
			System.out.println("would you like to shoot? 1 for yes 2 for exit");
			if(in.hasNextInt())
	    	  {
	    		  int choice = in.nextInt();
	    		  if (choice == 2)
	    			  return 0;
	    		  
	    		  else if(choice == 1){
		    		  
		    		  ZombieProduct z= ActiveZombies.get(0);
		  		  		z.takeDamage(dmg);
		  		  		
		  		  		ActiveZombies.remove(0);
		  		  		
		  		  	if(!z.dead){
		  		  		ActiveZombies.add(z);
		  		  	}
		  		  		
		    	  }
		    
		    	
	    	  }
			
		}
	}
	
}
