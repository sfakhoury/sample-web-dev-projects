
/**
 * @stereotype COMMANDER 
 */
public class Zombie {

	int Health;
	String name ="Z";
	boolean dead=false;
	
	
	/**
	 * @stereotype CONSTRUCTOR 
	 */
	public Zombie() {
		this.Health = 50;
	}
	
	
		
	/**
	 * @stereotype COMMAND LOCAL_CONTROLLER 
	 */
	public void takeDamage(int dmg)
	{
		this.Health -= dmg;
		
		if(die())
		{
			dead=true;
		}
		
	}
	/**
	 * @stereotype INCIDENTAL 
	 */
	public boolean die()
	{
		if(this.Health<=0)
		{
			return true;
		}
		return false;
	}
}
