
/**
 * @stereotype BOUNDARY 
 */
public class RegularZombieBuilder implements ZombieBuilder {
	Zombie z;
	
	
	/**
	 * @stereotype SET COLLABORATOR 
	 */
	public void CreateZombie() {
		Zombie z = new Zombie();
		this.z=z;
		
	}

	/**
	 * @stereotype EMPTY 
	 */
	@Override
	public void CreateAccesory() {
		
	}

	
	/**
	 * @stereotype FACTORY COLLABORATOR 
	 */
	public ZombieProduct zombie() {

		ZombieProduct product = new ZombieProduct(null, z);
		
		return product;
	}

	
}
