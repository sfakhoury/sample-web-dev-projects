
/**
 * @stereotype BOUNDARY 
 */
public class DoorZombieBuilder implements ZombieBuilder {
	Zombie z;
	Door Door;
	
	/**
	 * @stereotype SET COLLABORATOR 
	 */
	public void CreateZombie() {
		Zombie z = new Zombie();
		this.z = z;
		
	}

	/**
	 * @stereotype SET COLLABORATOR 
	 */
	@Override
	public void CreateAccesory() {
		Door Door = new Door();
		this.Door = Door;
		
	}

	
	/**
	 * @stereotype FACTORY COLLABORATOR 
	 */
	public ZombieProduct zombie() {
		
		ZombieProduct product = new ZombieProduct(Door, z);
		
		return product;
	}

	
}
