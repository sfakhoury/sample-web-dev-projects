
/**
 * @stereotype 
 */
abstract class AccessoryComponent {
	 String name;
	 int health;
	/**
	 * @stereotype ABSTRACT 
	 */
	public abstract boolean protect(int dmg);
	/**
	 * @stereotype ABSTRACT 
	 */
	public abstract boolean die();
}
