
/**
 * @stereotype BOUNDARY 
 */
public class ConeZombieBuilder implements ZombieBuilder {
	Zombie z;
	Cone cone;
	
	/**
	 * @stereotype SET COLLABORATOR 
	 */
	public void CreateZombie() {
		Zombie z = new Zombie();
		this.z = z;
		
	}

	/**
	 * @stereotype SET COLLABORATOR 
	 */
	@Override
	public void CreateAccesory() {
		Cone cone = new Cone();
		this.cone = cone;
		
	}

	
	/**
	 * @stereotype FACTORY COLLABORATOR 
	 */
	public ZombieProduct zombie() {
		
		ZombieProduct product = new ZombieProduct(cone, z);
		
		return product;
	}

	
}
