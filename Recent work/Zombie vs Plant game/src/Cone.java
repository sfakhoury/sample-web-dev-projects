
/**
 * @stereotype COMMANDER 
 */
public class Cone extends AccessoryComponent{
	
	boolean dead = false;
	
	/**
	 * @stereotype CONSTRUCTOR 
	 */
	public Cone() {
		super.health = 25;
		this.name = "C";
	}

	

	/**
	 * @stereotype SET 
	 */
	@Override
	public boolean die() {
		if (health <= 0){
			dead = true;
			return true;
		}
		return false;
	}



	/**
	 * @stereotype SET LOCAL_CONTROLLER 
	 */
	public boolean protect(int dmg) {
		
		if(die()){
			return false;
		}
		
		super.health -= dmg;
		
		return true;
	}








}
